define(
    ["jquery"],

    function(jquery) {
        "use strict";
        var movies = new Movies();
        movies.setRequestProcessor(jquery);
        return movies;
    }
);

function Movies() {
    "use strict";
    this.requestProcessor = null;
}
Movies.prototype.setRequestProcessor = function(requestProcessor) {
    "use strict";
    this.requestProcessor = requestProcessor;
};
Movies.prototype.getMoviesByRating = function(rating) {
    "use strict";
    return this.requestProcessor.ajax({
        type: "GET",
        url: "https://db.dev:6984/movies/_design/example/_view/by_rating",
        dataType: "json",
        data: {
            key: "\"" + rating + "\""
        }
    });
};
